<?php 
require dirname(__FILE__)."/adapter.php";
require dirname(__FILE__)."/hostpad.php";

$adapter = new CubieAdapter();
$hostpad = new Hostpad();
$config = new Config($adapter);

class Config {
   const FILE = 'channels.json';
   private $diff;
   private $adapter;
   public $channels;
   public $config;
   
   function __construct(Adapter $adapter) {   	
   	$this->adapter = $adapter;
   }
   
   function load(){
   	if(($this->config = json_decode($this->adapter->get())) 
			&& ($this->channels = array_unique($this->config->channels))){
			if(file_exists(self::FILE)){
				if($current = json_decode(file_get_contents(self::FILE))){
					if($this->diff = (array_diff($current, $this->channels) || array_diff($this->channels, $current))){
						file_put_contents(self::FILE, json_encode($this->channels));
					}
				}else{
					$this->diff = true;
					file_put_contents(self::FILE, json_encode($this->channels));
				}				
			}else{
				$this->diff = true;
				file_put_contents(self::FILE, json_encode($this->channels));
			}
		}else{
			if(file_exists(self::FILE)){
				if($current = json_decode(file_get_contents(self::FILE))){
					$this->diff = false;
					$this->channels = $current;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}		
		return true;
   }   
   
   function channels_changed() {   
       return $this->diff;
   }
   
   function get_bash_command() {   
       return $this->config->bash_command;
   }
}

if($config->load())
	echo "config loaded: ".print_r($config->channels, true)."\n";
else
	die('no configuration found');

$pids = shell_exec("/bin/pidof v2");
$pids = array_filter(explode(" ", trim($pids)));
	
if($config->channels_changed() || count($pids) != count($config->channels)){
	$ip = shell_exec("/sbin/ip link");
	$monitors = [];
	if(preg_match_all('/wlan[0-9]+/sm', $ip, $matches)){
		$monitors = array_diff($matches[0], [$hostpad->get_interface()]);
	}
	if(!$monitors)
		die('monitor interfaces not found');
		
	echo "restarting...\n";
	if($pids){
		shell_exec('/bin/kill '.join(' ', $pids));
		sleep(1);		
	}
	
	$min = min(count($monitors), count($config->channels));
	$m = array_combine(array_slice($monitors, 0, $min), array_slice($config->channels, 0, $min));
			
	$index = 0;
	foreach ($m as $wlan => $channel) {
		$scan = $index == 0 ? '-s' : '';
		shell_exec("./v2 -D -i $wlan -c $channel $scan");
		$index++;
		echo "$wlan: $channel $scan\n";
	}	
	if($skip = $hostpad->get_interface())
		echo "skipped: $skip\n";
		
	echo "$index started\n";
	
	sleep(3);
	$info = [];
	foreach ($m as $wlan => $channel) {
		$info[] = shell_exec("/sbin/iw $wlan info");
	}
	$adapter->post(json_encode(['info'=>$info]));		
}else{
	echo "channels are up to date\n";
}
	
if($command = $config->get_bash_command()){
	echo "executing command: $command\n";
	$result = shell_exec($command);
	$adapter->post(json_encode(['bash'=>['result'=>$result, 'command'=>$command]]));			
}