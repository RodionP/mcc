#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <assert.h> 
#include <vector> 
#include <vector> 
#include <boost/regex.hpp>
#include "utils.h"

using namespace std;

namespace utils{

	int frequencies[] = {2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447, 2452, 2457, 2462, 2467, 2472, 2484};

	std::string join( const std::vector<std::string>& elements, const char* const separator)
	{
		switch (elements.size())
		{
			case 0:
				return "";
			case 1:
				return elements[0];
			default:
				std::ostringstream os; 
				std::copy(elements.begin(), elements.end()-1, std::ostream_iterator<std::string>(os, separator));
				os << *elements.rbegin();
				return os.str();
		}
	}

	string get_eth_mac(){
		string line;
		ifstream file ("/sys/class/net/eth0/address");
		if (file.is_open())
		{
			getline(file, line);
			file.close();
		}
		return line;
	}


	int freq_to_channel(int freq){
		for (int i=0; i < 14; i++){
			if(frequencies[i] == freq){
				return i+1;
			}
		}
		return 0;
	}
	
	size_t write_to_string(void *contents, size_t size, size_t nmemb, void *userp)
	{
		size_t realsize = size * nmemb;
		if(realsize > 1000)
			realsize = 1000;
		struct MemoryStruct *mem = (struct MemoryStruct *)userp;

		mem->memory = reinterpret_cast<char*>(realloc(reinterpret_cast<void*>(mem->memory), mem->size + realsize + 1));
		if(mem->memory == NULL) {
			return 0;
		}

		memcpy(&(mem->memory[mem->size]), contents, realsize);
		mem->size += realsize;
		mem->memory[mem->size] = 0;

		return realsize;
	}

	vector<string> &split(const string &s, char delim, vector<string> &elems) {
		stringstream ss(s);
		string item;
		while (getline(ss, item, delim)) {
			elems.push_back(item);
		}
		return elems;
	}

	vector<string> split(const string &s, char delim) {
		vector<string> elems;
		split(s, delim, elems);
		return elems;
	}	
	
	string sha256(const char* data)
	{
		unsigned char hash[SHA256_DIGEST_LENGTH];
		SHA256_CTX sha256;
		SHA256_Init(&sha256);
		SHA256_Update(&sha256, data, strlen(data));
		SHA256_Final(hash, &sha256);
		
		char hex[3];
		ostringstream out;
		for(int i = 0; i < SHA256_DIGEST_LENGTH; i++)
		{
			sprintf(hex, "%02x", hash[i]);
			out << hex;
		}
		return out.str();
	}
	
	
	// trim from start
	std::string &ltrim(std::string &s) {
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
		return s;
	}

	// trim from end
	std::string &rtrim(std::string &s) {
		s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
		return s;
	}

	std::string &trim(std::string &s) {
		return ltrim(rtrim(s));
	}

	vector<string>
	resplit(const string & s, string rgx_str) {

	  vector<string> elems;

	  boost::regex rgx (rgx_str);

	  boost::sregex_token_iterator iter(s.begin(), s.end(), rgx, -1);
	  boost::sregex_token_iterator end;

	  while (iter != end)  {
		  elems.push_back(*iter);
		  ++iter;
	  }

	  return elems;
	}

	vector<string>
	match_substrings(const string & s, string rgx_str, int index) {

	  vector<string> elems;
	  boost::smatch matches;

	  boost::regex rgx (rgx_str);

	  boost::sregex_token_iterator iter(s.begin(), s.end(), rgx);
	  boost::sregex_token_iterator end;

	  while (iter != end)  {
		  const string m = *iter;
		  boost::regex_match(m, matches, rgx);
		  elems.push_back(matches[index]);
		  ++iter;
	  }  

	  return elems;
	}
	
	string exec(string cmd) {
		FILE* pipe = popen(cmd.c_str(), "r");
		if (!pipe) 
			return "";
		
		char buffer[128];
		std::string result = "";
		
		while(!feof(pipe)) {
			if(fgets(buffer, 128, pipe) != NULL)
				result += buffer;
		}
		pclose(pipe);
		return result;
	}
	
}