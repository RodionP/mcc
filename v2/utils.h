#include <string.h>
#include "openssl/sha.h"

using namespace std;

namespace utils{
	
	string join( const vector<string>& elements, const char* const separator);
	string get_eth_mac();
	int freq_to_channel(int freq);
	size_t write_to_string(void *contents, size_t size, size_t nmemb, void *userp);
	vector<string> &split(const string &s, char delim, vector<string> &elems);
	vector<string> split(const string &s, char delim);
	string sha256(const char* data);
	
	string &ltrim(string &s);
	string &rtrim(string &s);
	string &trim(string &s);
	vector<string> resplit(const string & s, string rgx_str);
	vector<string> match_substrings(const string & s, string rgx_str, int index);	
	string exec(string);
	
	struct MemoryStruct {
	  char *memory;
	  size_t size;
	};	
}