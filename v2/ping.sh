#!/bin/bash
host=$(cat ./domain)
mac=$(cat /sys/class/net/eth0/address)
ip=$(/sbin/ifconfig | grep -Pzo "^(eth0).*\n.*inet addr:(.*?)\s" | grep -Pzo "(?<=inet addr:)(.*?)\s")
wget -qO- "https://$host/admin/ping?m=$mac&ip=$ip"
