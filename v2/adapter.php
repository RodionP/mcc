<?php
class Adapter {   
	protected $url;
	protected $controller;
	protected $domain;

	protected function eth0_mac(){
		return trim(file_get_contents('/sys/class/net/eth0/address'));
	}

	function __construct() {
		if(!($this->domain = trim(file_get_contents('./domain')))){
			die('domain is empty');
		}					
		$uri=http_build_query(['m'=>$this->eth0_mac()]);
		$this->url="https://".$this->domain."/{$this->controller}/config/?$uri";
	}   

	function get(){
		$options = array(
			'http' => array(
				'method'  => 'GET',
			),
		);
		$context  = stream_context_create($options);
		return file_get_contents($this->url, false, $context);
	}

	function post($data){
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query(['result'=>$data]),
			),
		);
		$context  = stream_context_create($options);
		return file_get_contents($this->url, false, $context);
	}  
}

class CubieAdapter extends Adapter {
 	protected $controller = 'cubies';
}

class ApsAdapter extends Adapter {
 	protected $controller = 'aps';
}

class SyncAdapter extends Adapter {
 	protected $controller = 'aps';
 	
	function __construct() {
		parent::__construct();
		$uri=http_build_query([
			'm'=>$this->eth0_mac(),
			't'=>file_exists('timestamp') ? file_get_contents('timestamp') : null
		]);
		$this->url="https://".$this->domain."/{$this->controller}/sync/?$uri";
	}    	
}
