#include <string>


using namespace std;

struct LogLine {
    time_t time;
    string src;
    string dst;
    int rssi;
    int subtype;
    int type;
    string body;
};

u_int ieee802_11_if_print(const struct pcap_pkthdr *h, const u_char *p);
u_int ieee802_11_radio_if_print(const struct pcap_pkthdr *h, const u_char *p);

extern LogLine log_line;
