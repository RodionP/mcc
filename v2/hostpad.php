<?php
class Hostpad {
 	const FILE = '/etc/hostapd/hostapd.conf'; 
 	private $changed;
 	private $hostapd;
 	
	function __construct() {
		$this->hostapd = [];
		if(file_exists(self::FILE)){
			$items = file_get_contents(self::FILE);
			$items = explode("\n", trim($items));
			foreach ($items as $i) {
				list($n, $v) = explode("=", $i);
				$this->hostapd[$n] = $v;
			}
		}
	}   

	function set($config) {
		$this->changed = 0;
		if(in_array($config->channel, range(1, 14)) && $this->hostapd['channel'] != $config->channel){
			$this->hostapd['channel'] = $config->channel;
			$this->changed++;
		}

		if(preg_match('/^[A-Za-z0-9-._]+$/', $config->ssid) && $config->ssid != $this->hostapd['ssid']){
			$this->changed++;
			$this->hostapd['ssid'] = $config->ssid;
		}
	}
	
	function save() {	
		if($this->changed > 0){
			$c = [];
			foreach ($this->hostapd as $n => $v) {
				$c[] = "$n=$v";
			}
			file_put_contents(self::FILE, join("\n", $c));			
		}
		return $this->changed > 0;
	}
	
	function restart() {
		system('/usr/sbin/service hostapd stop');
		sleep(3);
		system('/usr/sbin/service hostapd start');
		sleep(3);
	}
	
	function get_interface() {	
	    return @$this->hostapd['interface'];
	}
	
	function get_channel() {	
	    return @$this->hostapd['channel'];
	}	
	
	function get_ssid() {	
	    return @$this->hostapd['ssid'];
	}		
}