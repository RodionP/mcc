#!/bin/bash
crontab -r
crontab -l > cron

pwd=$(pwd)

echo "* * * * * cd $pwd && /usr/bin/php ./alive.php" >> cron
echo "* * * * * cd $pwd && /usr/bin/php ./ap/config.php" >> cron
echo "* * * * * cd $pwd && ./ping.sh" >> cron
echo "0 1 * * * /sbin/reboot" >> cron

crontab cron
rm cron
