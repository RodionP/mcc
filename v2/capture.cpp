#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <pcap.h>
#include "cpack.h"
#include "radiotap-parser.h"
#include "ieee802_11_radio.h"
#include "ieee802_11.h"
#include "extract.h"
#include "capture.h"
#include "netdissect.h"

#ifndef roundup2
#define	roundup2(x, y)	(((x)+((y)-1))&(~((y)-1))) /* if y is powers of two */
#endif

LogLine log_line;

netdissect_options Gndo;
netdissect_options *gndo = &Gndo;

#define Hflag gndo->ndo_Hflag
#define snaplen     gndo->ndo_snaplen
#define snapend     gndo->ndo_snapend

static int
extract_header_length(u_int16_t fc)
{
	int len;

	switch (FC_TYPE(fc)) {
	case T_MGMT:
		return MGMT_HDRLEN;
	case T_CTRL:
		switch (FC_SUBTYPE(fc)) {
		case CTRL_BAR:
			return CTRL_BAR_HDRLEN;
		case CTRL_PS_POLL:
			return CTRL_PS_POLL_HDRLEN;
		case CTRL_RTS:
			return CTRL_RTS_HDRLEN;
		case CTRL_CTS:
			return CTRL_CTS_HDRLEN;
		case CTRL_ACK:
			return CTRL_ACK_HDRLEN;
		case CTRL_CF_END:
			return CTRL_END_HDRLEN;
		case CTRL_END_ACK:
			return CTRL_END_ACK_HDRLEN;
		default:
			return 0;
		}
	case T_DATA:
		len = (FC_TO_DS(fc) && FC_FROM_DS(fc)) ? 30 : 24;
		if (DATA_FRAME_IS_QOS(FC_SUBTYPE(fc)))
			len += 2;
		return len;
	default:
		printf("unknown IEEE802.11 frame type (%d)", FC_TYPE(fc));
		return 0;
	}
}

static int
extract_mesh_header_length(const u_char *p)
{
	return (p[0] &~ 3) ? 0 : 6*(1 + (p[0] & 3));
}

#define HASHNAMESIZE 4096

struct hnamemem {
	u_int32_t addr;
	const char *name;
	struct hnamemem *nxt;
};

struct enamemem {
	u_short e_addr0;
	u_short e_addr1;
	u_short e_addr2;
	const char *e_name;
	u_char *e_nsap;			/* used only for nsaptable[] */
#define e_bs e_nsap			/* for bytestringtable */
	struct enamemem *e_nxt;
};

static struct enamemem enametable[HASHNAMESIZE];

void
error(const char *fmt, ...)
{
	exit(1);
	/* NOTREACHED */
}

#define BUFSIZE 128

static inline struct enamemem *
lookup_emem(const u_char *ep)
{
	register u_int i, j, k;
	struct enamemem *tp;

	k = (ep[0] << 8) | ep[1];
	j = (ep[2] << 8) | ep[3];
	i = (ep[4] << 8) | ep[5];

	tp = &enametable[(i ^ j) & (HASHNAMESIZE-1)];
	while (tp->e_nxt)
		if (tp->e_addr0 == i &&
		    tp->e_addr1 == j &&
		    tp->e_addr2 == k)
			return tp;
		else
			tp = tp->e_nxt;
	tp->e_addr0 = i;
	tp->e_addr1 = j;
	tp->e_addr2 = k;
	tp->e_nxt = (struct enamemem *)calloc(1, sizeof(*tp));
	if (tp->e_nxt == NULL)
		error("lookup_emem: calloc");

	return tp;
}

static const char hex[] = "0123456789abcdef";


const char *
etheraddr_string(register const u_char *ep)
{
	register int i;
	register char *cp;
	register struct enamemem *tp;
	int oui;
	char buf[BUFSIZE];

	tp = lookup_emem(ep);
	if (tp->e_name)
		return (tp->e_name);
#ifdef USE_ETHER_NTOHOST
	if (!nflag) {
		char buf2[BUFSIZE];

		/*
		 * We don't cast it to "const struct ether_addr *"
		 * because some systems fail to declare the second
		 * argument as a "const" pointer, even though they
		 * don't modify what it points to.
		 */
		if (ether_ntohost(buf2, (struct ether_addr *)ep) == 0) {
			tp->e_name = strdup(buf2);
			return (tp->e_name);
		}
	}
#endif
	cp = buf;
	oui = EXTRACT_24BITS(ep);
	*cp++ = hex[*ep >> 4 ];
	*cp++ = hex[*ep++ & 0xf];
	for (i = 5; --i >= 0;) {
		*cp++ = ':';
		*cp++ = hex[*ep >> 4 ];
		*cp++ = hex[*ep++ & 0xf];
	}

    *cp = '\0';
	tp->e_name = strdup(buf);
	return (tp->e_name);
}

static void
mgmt_header_print(const u_char *p, const u_int8_t **srcp,
    const u_int8_t **dstp)
{
	const struct mgmt_header_t *hp = (const struct mgmt_header_t *) p;

	if (srcp != NULL)
		*srcp = hp->sa;
	if (dstp != NULL)
		*dstp = hp->da;

	log_line.src.assign(etheraddr_string((hp)->sa), 17);
	log_line.dst.assign(etheraddr_string((hp)->da), 17);
}

static void
ctrl_header_print(u_int16_t fc, const u_char *p, const u_int8_t **srcp,
    const u_int8_t **dstp)
{
	if (srcp != NULL)
		*srcp = NULL;
	if (dstp != NULL)
		*dstp = NULL;

	switch (FC_SUBTYPE(fc)) {
	case CTRL_BAR:
		log_line.src.assign(etheraddr_string(((const struct ctrl_bar_t *)p)->ta), 17);
		log_line.dst.assign(etheraddr_string(((const struct ctrl_bar_t *)p)->ra), 17);
		break;
	case CTRL_BA:
		break;
	case CTRL_PS_POLL:
		log_line.src.assign(etheraddr_string(((const struct ctrl_ps_poll_t *)p)->ta), 17);
		log_line.dst.assign(etheraddr_string(((const struct ctrl_ps_poll_t *)p)->bssid), 17);
		break;
	case CTRL_RTS:
		log_line.src.assign(etheraddr_string(((const struct ctrl_rts_t *)p)->ta), 17);
		log_line.dst.assign(etheraddr_string(((const struct ctrl_rts_t *)p)->ra), 17);
		break;
	case CTRL_CTS:
		break;
	case CTRL_ACK:
		break;
	case CTRL_CF_END:
		break;
	case CTRL_END_ACK:
		break;
	default:
		break;
	}
}

static void
data_header_print(u_int16_t fc, const u_char *p, const u_int8_t **srcp,
    const u_int8_t **dstp)
{

#define ADDR1  (p + 4)
#define ADDR2  (p + 10)
#define ADDR3  (p + 16)
#define ADDR4  (p + 24)

	if (!FC_TO_DS(fc) && !FC_FROM_DS(fc)) {
		if (srcp != NULL)
			*srcp = ADDR2;
		if (dstp != NULL)
			*dstp = ADDR1;
		/*if (!eflag)
			return;*/
		
		log_line.src.assign(etheraddr_string(ADDR2), 17);
		log_line.dst.assign(etheraddr_string(ADDR1), 17);
	} else if (!FC_TO_DS(fc) && FC_FROM_DS(fc)) {
		if (srcp != NULL)
			*srcp = ADDR3;
		if (dstp != NULL)
			*dstp = ADDR1;

		log_line.src.assign(etheraddr_string(ADDR3), 17);
		log_line.dst.assign(etheraddr_string(ADDR1), 17);
	} else if (FC_TO_DS(fc) && !FC_FROM_DS(fc)) {
		if (srcp != NULL)
			*srcp = ADDR2;
		if (dstp != NULL)
			*dstp = ADDR3;

		log_line.src.assign(etheraddr_string(ADDR2), 17);
		log_line.dst.assign(etheraddr_string(ADDR3), 17);
	} else if (FC_TO_DS(fc) && FC_FROM_DS(fc)) {
		if (srcp != NULL)
			*srcp = ADDR4;
		if (dstp != NULL)
			*dstp = ADDR3;

		log_line.src.assign(etheraddr_string(ADDR4), 17);
		log_line.dst.assign(etheraddr_string(ADDR3), 17);
	}

#undef ADDR1
#undef ADDR2
#undef ADDR3
#undef ADDR4
}

static void
ieee_802_11_hdr_print(u_int16_t fc, const u_char *p, u_int hdrlen,
    u_int meshdrlen, const u_int8_t **srcp, const u_int8_t **dstp)
{
	switch (FC_TYPE(fc)) {
	case T_MGMT:
		mgmt_header_print(p, srcp, dstp);
		break;
	case T_CTRL:
		ctrl_header_print(fc, p, srcp, dstp);
		break;
	case T_DATA:
		data_header_print(fc, p, srcp, dstp);
		break;
	default:
		*srcp = NULL;
		*dstp = NULL;
		break;
	}
}

#define TTEST2(var, l) (snapend - (l) <= snapend && \
			(const u_char *)&(var) <= snapend - (l))

#define NTOHS(x)	(x) = ntohs(x)


static u_int ieee802_11_print(const u_char *p, u_int length, u_int orig_caplen, int pad, 
							  u_int fcslen)
{
	u_int16_t fc;
	u_int caplen, hdrlen, meshdrlen;
	const u_int8_t *src, *dst;

	caplen = orig_caplen;
	/* Remove FCS, if present */
	if (length < fcslen) {
		return caplen;
	}
	length -= fcslen;
	if (caplen > length) {
		/* Amount of FCS in actual packet data, if any */
		fcslen = caplen - length;
		caplen -= fcslen;
		snapend -= fcslen;
	}

	if (caplen < IEEE802_11_FC_LEN) {
		return orig_caplen;
	}

	fc = EXTRACT_LE_16BITS(p);
	hdrlen = extract_header_length(fc);
	if (pad)
		hdrlen = roundup2(hdrlen, 4);
	if (Hflag && FC_TYPE(fc) == T_DATA &&
	    DATA_FRAME_IS_QOS(FC_SUBTYPE(fc))) {
		meshdrlen = extract_mesh_header_length(p+hdrlen);
		hdrlen += meshdrlen;
	} else
		meshdrlen = 0;


	if (caplen < hdrlen) {
		return hdrlen;
	}

	ieee_802_11_hdr_print(fc, p, hdrlen, meshdrlen, &src, &dst);

	log_line.subtype = FC_SUBTYPE(fc);
	log_line.type = FC_TYPE(fc);
	
	return hdrlen;
}

/*
 * This is the top level routine of the printer.  'p' points
 * to the 802.11 header of the packet, 'h->ts' is the timestamp,
 * 'h->len' is the length of the packet off the wire, and 'h->caplen'
 * is the number of bytes actually captured.
 */
u_int
ieee802_11_if_print(const struct pcap_pkthdr *h, const u_char *p)
{
	return ieee802_11_print(p, h->len, h->caplen, 0, 0);
}

//**********************************radio**********************************

struct radiotap_state
{
	u_int32_t	present;

	u_int8_t	rate;
};

#define MAX_MCS_INDEX	76

#define	IEEE80211_CHAN_FHSS \
	(IEEE80211_CHAN_2GHZ | IEEE80211_CHAN_GFSK)
#define	IEEE80211_CHAN_A \
	(IEEE80211_CHAN_5GHZ | IEEE80211_CHAN_OFDM)
#define	IEEE80211_CHAN_B \
	(IEEE80211_CHAN_2GHZ | IEEE80211_CHAN_CCK)
#define	IEEE80211_CHAN_PUREG \
	(IEEE80211_CHAN_2GHZ | IEEE80211_CHAN_OFDM)
#define	IEEE80211_CHAN_G \
	(IEEE80211_CHAN_2GHZ | IEEE80211_CHAN_DYN)

#define	IS_CHAN_FHSS(flags) \
	((flags & IEEE80211_CHAN_FHSS) == IEEE80211_CHAN_FHSS)
#define	IS_CHAN_A(flags) \
	((flags & IEEE80211_CHAN_A) == IEEE80211_CHAN_A)
#define	IS_CHAN_B(flags) \
	((flags & IEEE80211_CHAN_B) == IEEE80211_CHAN_B)
#define	IS_CHAN_PUREG(flags) \
	((flags & IEEE80211_CHAN_PUREG) == IEEE80211_CHAN_PUREG)
#define	IS_CHAN_G(flags) \
	((flags & IEEE80211_CHAN_G) == IEEE80211_CHAN_G)
#define	IS_CHAN_ANYG(flags) \
	(IS_CHAN_PUREG(flags) || IS_CHAN_G(flags))

static void
print_chaninfo(int freq, int flags)
{
	printf("%u MHz", freq);
	if (IS_CHAN_FHSS(flags))
		printf(" FHSS");
	if (IS_CHAN_A(flags)) {
		if (flags & IEEE80211_CHAN_HALF)
			printf(" 11a/10Mhz");
		else if (flags & IEEE80211_CHAN_QUARTER)
			printf(" 11a/5Mhz");
		else
			printf(" 11a");
	}
	if (IS_CHAN_ANYG(flags)) {
		if (flags & IEEE80211_CHAN_HALF)
			printf(" 11g/10Mhz");
		else if (flags & IEEE80211_CHAN_QUARTER)
			printf(" 11g/5Mhz");
		else
			printf(" 11g");
	} else if (IS_CHAN_B(flags))
		printf(" 11b");
	if (flags & IEEE80211_CHAN_TURBO)
		printf(" Turbo");
	if (flags & IEEE80211_CHAN_HT20)
		printf(" ht/20");
	else if (flags & IEEE80211_CHAN_HT40D)
		printf(" ht/40-");
	else if (flags & IEEE80211_CHAN_HT40U)
		printf(" ht/40+");
	printf(" ");
}

static int
print_radiotap_field(struct cpack_state *s, u_int32_t bit, u_int8_t *flags,
						struct radiotap_state *state, u_int32_t presentflags)
{
	union {
		int8_t		i8;
		u_int8_t	u8;
		int16_t		i16;
		u_int16_t	u16;
		u_int32_t	u32;
		u_int64_t	u64;
	} u, u2, u3, u4;
	int rc;

	switch (bit) {
	case IEEE80211_RADIOTAP_FLAGS:
		rc = cpack_uint8(s, &u.u8);
		if (rc != 0)
			break;
		*flags = u.u8;
		break;
	case IEEE80211_RADIOTAP_RATE:
		rc = cpack_uint8(s, &u.u8);
		if (rc != 0)
			break;

		/* Save state rate */
		state->rate = u.u8;
		break;
	case IEEE80211_RADIOTAP_DB_ANTSIGNAL:
	case IEEE80211_RADIOTAP_DB_ANTNOISE:
	case IEEE80211_RADIOTAP_ANTENNA:
		rc = cpack_uint8(s, &u.u8);
		break;
	case IEEE80211_RADIOTAP_DBM_ANTSIGNAL:
	case IEEE80211_RADIOTAP_DBM_ANTNOISE:
		rc = cpack_int8(s, &u.i8);
		break;
	case IEEE80211_RADIOTAP_CHANNEL:
		rc = cpack_uint16(s, &u.u16);
		if (rc != 0)
			break;
		rc = cpack_uint16(s, &u2.u16);
		break;
	case IEEE80211_RADIOTAP_FHSS:
	case IEEE80211_RADIOTAP_LOCK_QUALITY:
	case IEEE80211_RADIOTAP_TX_ATTENUATION:
	case IEEE80211_RADIOTAP_RX_FLAGS:
		rc = cpack_uint16(s, &u.u16);
		break;
	case IEEE80211_RADIOTAP_DB_TX_ATTENUATION:
		rc = cpack_uint8(s, &u.u8);
		break;
	case IEEE80211_RADIOTAP_DBM_TX_POWER:
		rc = cpack_int8(s, &u.i8);
		break;
	case IEEE80211_RADIOTAP_TSFT:
		rc = cpack_uint64(s, &u.u64);
		break;
	case IEEE80211_RADIOTAP_XCHANNEL:
		rc = cpack_uint32(s, &u.u32);
		if (rc != 0)
			break;
		rc = cpack_uint16(s, &u2.u16);
		if (rc != 0)
			break;
		rc = cpack_uint8(s, &u3.u8);
		if (rc != 0)
			break;
		rc = cpack_uint8(s, &u4.u8);
		break;
	case IEEE80211_RADIOTAP_MCS:
		rc = cpack_uint8(s, &u.u8);
		if (rc != 0)
			break;
		rc = cpack_uint8(s, &u2.u8);
		if (rc != 0)
			break;
		rc = cpack_uint8(s, &u3.u8);
		break;
	case IEEE80211_RADIOTAP_VENDOR_NAMESPACE: {
		u_int8_t vns[3];
		u_int16_t length;
		u_int8_t subspace;

		if ((cpack_align_and_reserve(s, 2)) == NULL) {
			rc = -1;
			break;
		}

		rc = cpack_uint8(s, &vns[0]);
		if (rc != 0)
			break;
		rc = cpack_uint8(s, &vns[1]);
		if (rc != 0)
			break;
		rc = cpack_uint8(s, &vns[2]);
		if (rc != 0)
			break;
		rc = cpack_uint8(s, &subspace);
		if (rc != 0)
			break;
		rc = cpack_uint16(s, &length);
		if (rc != 0)
			break;

		/* Skip up to length */
		s->c_next += length;
		break;
	}
	default:
		/* this bit indicates a field whose
		 * size we do not know, so we cannot
		 * proceed.  Just print the bit number.
		 */
		return -1;
	}

	if (rc != 0) {
		return rc;
	}

	/* Preserve the state present flags */
	state->present = presentflags;

	switch (bit) {
	case IEEE80211_RADIOTAP_DBM_ANTSIGNAL:
        log_line.rssi = u.i8;
		break;
	}

	return 0;
}


static u_int
ieee802_11_radio_print(const u_char *p, u_int length, u_int caplen)
{
#define	BITNO_32(x) (((x) >> 16) ? 16 + BITNO_16((x) >> 16) : BITNO_16((x)))
#define	BITNO_16(x) (((x) >> 8) ? 8 + BITNO_8((x) >> 8) : BITNO_8((x)))
#define	BITNO_8(x) (((x) >> 4) ? 4 + BITNO_4((x) >> 4) : BITNO_4((x)))
#define	BITNO_4(x) (((x) >> 2) ? 2 + BITNO_2((x) >> 2) : BITNO_2((x)))
#define	BITNO_2(x) (((x) & 2) ? 1 : 0)
#define	BIT(n)	(1U << n)
#define	IS_EXTENDED(__p)	\
	    (EXTRACT_LE_32BITS(__p) & BIT(IEEE80211_RADIOTAP_EXT)) != 0

	struct cpack_state cpacker;
	struct ieee80211_radiotap_header *hdr;
	u_int32_t present, next_present;
	u_int32_t presentflags = 0;
	u_int32_t *presentp, *last_presentp;
	enum ieee80211_radiotap_type bit;
	int bit0;
	const u_char *iter;
	u_int len;
	u_int8_t flags;
	int pad;
	u_int fcslen;
	struct radiotap_state state;

	if (caplen < sizeof(*hdr)) {
		return caplen;
	}

	hdr = (struct ieee80211_radiotap_header *)p;

	len = EXTRACT_LE_16BITS(&hdr->it_len);

	if (caplen < len) {
		return caplen;
	}
	for (last_presentp = &hdr->it_present;
	     IS_EXTENDED(last_presentp) &&
	     (u_char*)(last_presentp + 1) <= p + len;
	     last_presentp++);

	/* are there more bitmap extensions than bytes in header? */
	if (IS_EXTENDED(last_presentp)) {
		return caplen;
	}

	iter = (u_char*)(last_presentp + 1);

	if (cpack_init(&cpacker, (u_int8_t*)iter, len - (iter - p)) != 0) {
		/* XXX */
		return caplen;
	}

	/* Assume no flags */
	flags = 0;
	/* Assume no Atheros padding between 802.11 header and body */
	pad = 0;
	/* Assume no FCS at end of frame */
	fcslen = 0;
	for (bit0 = 0, presentp = &hdr->it_present; presentp <= last_presentp;
	     presentp++, bit0 += 32) {
		presentflags = EXTRACT_LE_32BITS(presentp);

		/* Clear state. */
		memset(&state, 0, sizeof(state));

		for (present = EXTRACT_LE_32BITS(presentp); present;
		     present = next_present) {
			/* clear the least significant bit that is set */
			next_present = present & (present - 1);

			/* extract the least significant bit that is set */
			bit = (enum ieee80211_radiotap_type)
			    (bit0 + BITNO_32(present ^ next_present));

			if (print_radiotap_field(&cpacker, bit, &flags, &state, presentflags) != 0)
				goto out;
		}
	}

out:
	if (flags & IEEE80211_RADIOTAP_F_DATAPAD)
		pad = 1;	/* Atheros padding */
	if (flags & IEEE80211_RADIOTAP_F_FCS)
		fcslen = 4;	/* FCS at end of packet */
	return len + ieee802_11_print(p + len, length - len, caplen - len, pad,
	    fcslen);
#undef BITNO_32
#undef BITNO_16
#undef BITNO_8
#undef BITNO_4
#undef BITNO_2
#undef BIT
}


u_int
ieee802_11_radio_if_print(const struct pcap_pkthdr *h, const u_char *p)
{
	return ieee802_11_radio_print(p, h->len, h->caplen);
}



