#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <sstream>
#include <time.h>
#include <list>
#include <algorithm>
#include <curl/curl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/file.h>
#include <fcntl.h>
#include <pthread.h>
#include "utils.h"
#include "zlib.h"
#include <math.h>
#include <boost/regex.hpp>
#include <boost/format.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

#include <pcap.h>
#include "radiotap-parser.h"
#include "capture.h"
#include "ieee802_11.h"

using namespace std;

const int UPLOAD_TIME_INTERVAL = 60;
const int FLUSH_TIME_INTERVAL = 15;
const int LOAD_CONFIG_INTERVAL = 5*60;
const int SCAN_INTERVAL = 5*60;
const int CHECK_INTERVAL = 60;
const int RING_BUFFER_MAX_SIZE = UPLOAD_TIME_INTERVAL * 2 * 100;
const int MAX_PACKETS_PER_SECOND = 100;
const int PCAP_BUFFER_SIZE = 1024 * 1024;
const int MAX_LOG_SIZE = 2 * 1024 * 1024;
const int LOG_COPY_BUF_SIZE = 8 * 512;

class Config {
private:
	string mac;
public:
	string post_url;
	string scan_url;
	string config_url;
	string interface;
	int channel;
	bool debug;
	bool scan;

	Config(){
		mac = utils::get_eth_mac();
		std::ifstream file("domain");
		string domain;
		std::getline(file, domain);
		file.close();
		config_url = "https://" + domain + "/api/cubie_get_config";
	}
	
	string get_config_url(){		
		return (boost::format("%1%?m=%2%") % config_url % mac).str();
	}

	string get_post_url(){
		return (boost::format("%1%?m=%2%&c=%3%") % post_url % mac % channel).str();
	}
	
	string get_scan_url(){
		return (boost::format("%1%?m=%2%") % scan_url % mac).str();
	}
	
	void set_channel() {
		system((boost::format("/sbin/iwconfig %1% channel %2%") % interface % channel).str().c_str());
	}
};
Config config;

struct HeapRow {
	time_t created;
	char src[17];
	int rssi;
	char dst[17];
};

class LogFile {
private:
	int fd;
	pthread_mutex_t mutex;

	void reopen(){
		fd = open("log", O_CREAT | O_APPEND | O_WRONLY, 0666);
		if (fd == -1)
			cout << "log error: " << strerror(errno) << "\n";		
	}
	
	void check() {
		int size = lseek(fd, 0, SEEK_END);
		if(size < MAX_LOG_SIZE)
			return;
				
		close(fd);	

		int hl = open("~log", O_CREAT | O_WRONLY | O_TRUNC, 0666);
		if (hl == -1){
			cout << "log error: " << strerror(errno) << "\n";
			return;
		}

		int l = open("log", O_RDONLY, 0666);
		if (l == -1){
			cout << "log error: " << strerror(errno) << "\n";
			return;
		}		

		lseek(l, size/2, SEEK_SET);		
		char buf[LOG_COPY_BUF_SIZE];
		ssize_t nread;

		int total = 0;
		while ((nread = read(l, buf, sizeof buf)) && nread > 0 && total < size/2) {
			write(hl, buf, nread);
			total += nread;
		}
				
		close(hl);
		close(l);
		
		hl = open("~log", O_RDONLY, 0666);
		if (hl == -1){
			cout << "log error: " << strerror(errno) << "\n";
			return;
		}

		l = open("log", O_WRONLY | O_TRUNC, 0666);
		if (l == -1){
			cout << "log error: " << strerror(errno) << "\n";
			return;
		}				

		while ((nread = read(hl, buf, sizeof buf)) && nread > 0) {
			write(l, buf, nread);
		}
		
		close(hl);
		close(l);
		unlink("~log");
		reopen();

		string m = (boost::format("log trimmed. start size: %1%, new size: %2%\n") % size % total).str();
		if (config.debug)
			cout << m;

		header();
		*(this) << m;
	}
	string timestamp() {
		time_t now = time(0);
		struct tm tstruct;
		char buf[80];
		tstruct = *localtime(&now);
		strftime(buf, sizeof (buf), "%Y-%m-%d %X", &tstruct);
		string dt = buf;
		return dt;
	}
public:
	LogFile() {
		pthread_mutex_init(&mutex, NULL);
	};

	LogFile& operator<<(string s) {
		if (config.debug)
			cout << s;

		if(write(fd, s.c_str(), s.size()) == -1){
			if (config.debug)			
				cout << "log error: " << strerror(errno) << "\n";
		}			
		return *this;
	};

	LogFile& operator<<(int i) {
		stringstream ss;
		ss << i;
		*(this) << ss.str();
		return *this;
	};

	void header(){
		*(this) << config.interface << " " << config.channel << " " << timestamp() << "\t";			
	}
	
	void start() {
		pthread_mutex_lock(&mutex);
		reopen();
		flock(fd, LOCK_EX);
		check();
		header();
	};

	void finish() {
		pthread_mutex_unlock(&mutex);
		flock(fd, LOCK_UN);
		close(fd);
	}

	void print(string s) {
		start();
		*(this) << s << "\n";
		finish();
	}
};

LogFile logfile;

class Filter {
private:
	vector<string> macs;
	string file_name;

public:

	Filter(string fname) {
		file_name = fname;

		std::ifstream file(file_name.c_str());
		std::string line;

		while (std::getline(file, line)) {
			macs.push_back(line);
		}
		file.close();

		std::sort(macs.begin(), macs.end());
		macs.erase(unique(macs.begin(), macs.end()), macs.end());
	}

	bool is_in(string source) {
		return std::find(macs.begin(), macs.end(), source) != macs.end();
	}

	void push_back(string source) {
		ofstream ofs;

		ofs.open(file_name.c_str(), std::ios_base::app);
		ofs << source << "\n";
		ofs.close();

		macs.push_back(source);
		logfile.print(source + " added to " + file_name);
	}
};

class Aps : public Filter {
public:

	Aps() : Filter("aps") {
	}
};

class Night : public Filter {
public:

	Night() : Filter("night") {
	}

	bool is_now() {
		char buffer [5];
		time_t rawtime;
		struct tm * timeinfo;
		time(&rawtime);
		timeinfo = localtime(&rawtime);
		strftime(buffer, 5, "%H", timeinfo);
		int hour = atoi(buffer);

		return 0 <= hour && hour <= 5;
	}
};

class RingBuffer {
private:
	pthread_mutex_t mutex;
	HeapRow data[RING_BUFFER_MAX_SIZE];
public:
	int size;

	RingBuffer() {
		pthread_mutex_init(&mutex, NULL);
	}
	void push_back(HeapRow hr) {
		lock();

		if (size == RING_BUFFER_MAX_SIZE) {
			logfile.print("ring buffer max size reached");
			size = 0;
		}
		data[size] = hr;
		size++;

		unlock();
	}
	void lock() {
		pthread_mutex_lock(&mutex);
	}
	void unlock() {
		pthread_mutex_unlock(&mutex);
	}
	string copy(){
		stringstream ss;
		for(int i=0; i<size; i++){
			ss << data[i].created << "," << data[i].src << "," << data[i].rssi << "," << data[i].dst << "\n";
		}
		size = 0;
		return ss.str();
	}
};


RingBuffer heap;
vector<string> filter;

class Collector {
private:
	map<time_t, map<string, vector<LogLine> > > buf;
	Aps aps;
	Night night;
	vector<string> unique_sources;

	bool is_filtered(string mac) {
		return std::find(filter.begin(), filter.end(), mac) != filter.end();
	}

	vector<string> parse_ip_addr(string s) {
		s = utils::trim(s);

		vector<string> rows = utils::resplit(s, "[0-9]: [a-z0-9.]+: ");

		rows.erase(rows.begin());

		vector<string> interfaces = utils::match_substrings(s, "[0-9]: ([a-z0-9.]+): ", 1);

		map<string, string> ip_addr;

		vector<string>::iterator irows = rows.begin();
		vector<string>::iterator iinter = interfaces.begin();

		while (irows != rows.end()) {
			ip_addr[(*iinter)] = *irows;

			irows++;
			iinter++;
		}

		boost::smatch matches;
		boost::regex rgx("ether ([a-z0-9:]+) ");
		vector<string> macs;

		for (map<string, string>::iterator it = ip_addr.begin(); it != ip_addr.end(); ++it) {
			if (it->first.find("wlan") != string::npos) {
				if (boost::regex_search(it->second, matches, rgx))
					macs.push_back(matches[1]);
			}
		}

		sort(macs.begin(), macs.end());
		macs.erase(unique(macs.begin(), macs.end()), macs.end());

		return macs;
	}

public:
	int counter;
	
	Collector() {
		counter = 0;
		vector<string> macs(parse_ip_addr(utils::exec("/sbin/ip addr")));
		if (macs.size() > 0)
			filter.insert(filter.end(), macs.begin(), macs.end());
	}

	void process(LogLine log_line) {
		static time_t flush_time = time(0) + FLUSH_TIME_INTERVAL;
		static int collected_count, filtered_count, aps_count, dropped_count = 0;
		counter++;
		
		if (time(0) >= flush_time) {
			for (map<time_t, map<string, vector<LogLine> > >::iterator time_it = buf.begin(); time_it != buf.end(); ++time_it) {

				for (map<string, vector<LogLine> >::iterator source_it = time_it->second.begin();
						  source_it != time_it->second.end(); ++source_it) {
					HeapRow hr;
					memset(&hr, 0, sizeof (HeapRow));

					hr.created = time_it->first;
					memcpy(hr.src, source_it->first.c_str(), source_it->first.size());
					memcpy(hr.dst, source_it->second.front().dst.c_str(), source_it->second.front().dst.size());

					int sum = 0;
					for (vector<LogLine>::iterator line_it = source_it->second.begin();
							  line_it != source_it->second.end(); ++line_it) {
						sum += line_it->rssi;
					}
					hr.rssi = round((double) sum / source_it->second.size());

					heap.push_back(hr);
				}
			}

			char msg[100];
			sprintf(msg, "collected %d, filtered %d, aps %d, dropped %d, unique sources %d",
					  collected_count, filtered_count, aps_count, dropped_count, unique_sources.size());
			logfile.print(msg);

			collected_count = filtered_count = aps_count = dropped_count = 0;
			buf.clear();
			unique_sources.clear();
			
			flush_time = time(0) + FLUSH_TIME_INTERVAL;
		}		
		
		if (log_line.src == "")
			return;

		if(log_line.src == "00:00:00:00:00:00")
			return;
		
		if (aps.is_in(log_line.src)) {
			aps_count++;
			return;
		}

		if (is_filtered(log_line.src)) {
			filtered_count++;
			return;
		}

		if ((log_line.type == T_MGMT && log_line.subtype == ST_PROBE_RESPONSE) ||
				  (log_line.type == T_MGMT && log_line.subtype == ST_BEACON)) {
			aps.push_back(log_line.src);
			aps_count++;
			return;
		}

		if (buf[log_line.time][log_line.src].size() < MAX_PACKETS_PER_SECOND) {
			buf[log_line.time][log_line.src].push_back(log_line);
			collected_count++;

			if (find(unique_sources.begin(), unique_sources.end(), log_line.src) == unique_sources.end()) {
				unique_sources.push_back(log_line.src);
			}
		} else {
			dropped_count++;
		}		
	}
};

Collector collector;

void pcap_callback(u_char *args, const struct pcap_pkthdr* pkthdr, const u_char*packet) {
	log_line.rssi = 0;
	log_line.src = "";
	log_line.subtype = 0;
	log_line.type = 0;
	log_line.time = time(0);

	struct ieee80211_radiotap_header *rth = (struct ieee80211_radiotap_header *) packet;

	ieee802_11_radio_if_print(pkthdr, packet);
	ieee802_11_if_print(pkthdr, packet + rth->it_len);

	collector.process(log_line);
};

class Pcap {
private:
	pcap_t *handler;	
public:	
	void start(){
		char errbuf[PCAP_ERRBUF_SIZE];
		char* errbuff;

		handler = pcap_create(config.interface.c_str(), errbuff);
		if (handler == NULL) {
			logfile.start();
			logfile << "pcap_create error: " << errbuf << "\n";
			logfile.finish();
			return;
		}
		if (pcap_set_rfmon(handler, 1) != 0) {
			logfile.start();
			logfile << "can't change to monitor mode: " << config.interface << "\n";
			logfile.finish();
			return;
		}
		pcap_set_snaplen(handler, 2048);
		pcap_set_timeout(handler, 512);
		pcap_set_promisc(handler, 0);
		pcap_set_buffer_size(handler, PCAP_BUFFER_SIZE);

		int code = pcap_activate(handler);
		if (code != 0) {
			logfile.start();
			logfile << "pcap_activate failed. Error: " << pcap_geterr(handler) << "\n";
			logfile.finish();
			return;
		}

		config.set_channel();

		if (pcap_loop(handler, 0, pcap_callback, NULL) == -1) {
			logfile.start();
			logfile << "pcap_loop error: " << pcap_geterr(handler) << "\n";
			logfile.finish();
		}		
	}
	
	void stop(){
		if(handler){
			pcap_breakloop(handler);
			//pcap_close(handler);
		}
	}
};

Pcap pcap;

void * capture_packets_on_interface(void *) {
	pcap.start();
	return NULL;
}

class ZipAdapter{
public:
	string response;
	int zipped_size;
	
	bool get(string url){
		CURL *curl;
		CURLcode res;

		struct utils::MemoryStruct chunk;
		chunk.memory = reinterpret_cast<char*> (malloc(1));
		chunk.size = 0;

		curl = curl_easy_init();

		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, utils::write_to_string);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &chunk);
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 20L);

		res = curl_easy_perform(curl);
		if (CURLE_OK != res) {
			logfile.start();
			logfile << "curl_easy_perform() failed: " << curl_easy_strerror(res) << "\n" << url << "\n\n";
			logfile.finish();				
		}
		response.assign(chunk.memory);
		curl_easy_cleanup(curl);

		if (chunk.memory)
			free(chunk.memory);
		
		return CURLE_OK == res;	
	}
	
	bool post(string url, const char* field, const Bytef* inBuffer, uLong inSize){			
		uLong outBufferSize = compressBound(inSize);
		Bytef* outBuffer = reinterpret_cast<Bytef*> (malloc(outBufferSize));

		compress2(outBuffer, &outBufferSize, inBuffer, inSize, Z_BEST_COMPRESSION);		
		
		CURL *curl;
		CURLcode res;
		
		struct utils::MemoryStruct chunk;
		chunk.memory = reinterpret_cast<char*> (malloc(1));
		chunk.size = 0;

		curl = curl_easy_init();
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, utils::write_to_string);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &chunk);
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 20L);

		struct curl_httppost* post = NULL;
		struct curl_httppost* last = NULL;

		curl_formadd(&post, &last,
				  CURLFORM_COPYNAME, field,
				  CURLFORM_PTRCONTENTS, outBuffer,
				  CURLFORM_CONTENTSLENGTH, outBufferSize,
				  CURLFORM_END);
		curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);

		res = curl_easy_perform(curl);
		if (res != CURLE_OK) {
			logfile.start();
			logfile << "curl_easy_perform() failed: " << curl_easy_strerror(res) << "\n" << url << "\n\n";
			logfile.finish();
		}
		response.assign(chunk.memory);
		zipped_size = outBufferSize;
		
		curl_easy_cleanup(curl);
		curl_formfree(post);
		free(outBuffer);

		if (chunk.memory)
			free(chunk.memory);		
		
		return res == CURLE_OK;			
	}
};

bool load_config() {
	ZipAdapter adapter;
	string url = config.get_config_url();		
	bool valid = true;
	
	if(adapter.get(url)){
      logfile.start();
		logfile << "config loaded:\t" << url << "\n\n";
	   logfile.finish();
		try
		{		
			stringstream ss(adapter.response);
			boost::property_tree::ptree pt;
			boost::property_tree::read_json(ss, pt);

			if((config.post_url = pt.get<string>("post_url")).empty()){
				valid = false;
				logfile.print("json error: post_url doesn't exist");
			}			

			if((config.scan_url = pt.get<string>("scan_url")).empty()){
				valid = false;
				logfile.print("json error: scan_url doesn't exist");
			}			

			string f;
			if(!(f = pt.get<string>("filter")).empty()){
			vector<string> splitted = utils::split(f, ',');
				for (vector<string>::iterator it = splitted.begin(); it != splitted.end(); ++it) {
					filter.push_back((*it));
				}		
			}		
		}
		catch (std::exception const& e)
		{
			valid = false;
			logfile.print((boost::format("json parse error: %1%") % e.what()).str());
		}		
	}else{
		valid = false;
	}
	return valid;
}

void upload_log() {	
	if (!heap.size) {
		logfile.print("heap is empty");
		return;
	}
	
	int rows = heap.size;
	ZipAdapter adapter;
	string url = config.get_post_url();	
	string body = heap.copy();
	
	if(adapter.post(url, "heap", reinterpret_cast<const Bytef*> (body.c_str()), body.size())){
		char msg[100];
		sprintf(msg, "log uploaded: size %d, ziped %d, rows %d", body.size(), adapter.zipped_size, rows);
		logfile.start();
		logfile << msg << "\nresp:" << adapter.response << "\n" << url << "\n\n";
		logfile.finish();						
	}
}

void scan(){	
	string out = utils::exec((boost::format("/sbin/ifconfig %1% down && /sbin/iwconfig %1% mode managed "
			  "&& /sbin/ifconfig %1% up && /sbin/iw %1% scan") % config.interface).str());

	ZipAdapter adapter;
	string url = config.get_scan_url();
	
	if(adapter.post(url, "scan", reinterpret_cast<const Bytef*> (out.c_str()), out.size())){
		char msg[100];
		sprintf(msg, "scan data send: original size %d, ziped %d", out.size(), adapter.zipped_size);
		logfile.start();
		logfile << msg << "\nresp:" << adapter.response << "\n" << url << "\n\n";
		logfile.finish();		
	}
}

int main(int argc, char **argv) {
	int opt;
	bool deamonize = false;
	
	while ((opt = getopt(argc, argv, "sdDi:c:l:")) != -1) {
		switch (opt) {
			case 'd':
				config.debug = true;
				break;
			case 'D':
				deamonize = true;
				break;
			case 'i':
				config.interface.assign(optarg);
				break;
			case 'c':
				config.channel = atoi(optarg);
				break;
			case 'l':
				config.config_url.assign(optarg);
				break;
			case 's':
				config.scan = true;
				break;
			default:
				fprintf(stderr, "Usage: %s [-d debug] [-D daemonize] [-l config url] [-s scan] -i interface -c channel\n", argv[0]);
				exit(EXIT_FAILURE);
		}
	}

	if (deamonize)
		daemon(1, config.debug);

	int load_config_time = 0;
	int scan_time = 0;
	int upload_time = time(0) + UPLOAD_TIME_INTERVAL;
	int check_time = time(0) + CHECK_INTERVAL;

	bool thread_running = false;
	pthread_t th;
	
	if(!config.scan)
		thread_running = pthread_create(&th, NULL, capture_packets_on_interface, NULL) == 0;
	
	while (true) {
		if (time(0) >= load_config_time) {
			load_config();
			load_config_time = time(0) + LOAD_CONFIG_INTERVAL;
		}
		if (config.scan && time(0) >= scan_time) {
			pcap.stop();
			if(!thread_running || !pthread_join(th, NULL)) {								
				scan();
				thread_running = pthread_create(&th, NULL, capture_packets_on_interface, NULL) == 0;
			}									
			scan_time = time(0) + SCAN_INTERVAL;
		}
		if (time(0) >= upload_time) {
			upload_log();
			upload_time = time(0) + UPLOAD_TIME_INTERVAL;
		}
		if (time(0) >= check_time) {
			if(collector.counter == 0){
				logfile.print(config.interface + " is stopped working. restarting");
				pcap.stop();
				if(!thread_running || !pthread_join(th, NULL)) {								
					system((boost::format("/sbin/ifconfig %1% down && /sbin/iwconfig %1% "
							  "mode monitor && /sbin/ifconfig %1% up") % config.interface).str().c_str());
					pthread_create(&th, NULL, capture_packets_on_interface, NULL);
				}						
			}else{
				collector.counter = 0;
			}
			check_time = time(0) + CHECK_INTERVAL;
		}
		sleep(1);
	}
	return 0;
}
