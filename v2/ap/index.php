<?php
//error_reporting(E_ALL);
error_reporting(0);

class UserMac {
	const LOG_FILE = 'users';
	const CHAIN = 'portal';
	private $mac;

	function __construct() {
		$mac = shell_exec("/usr/sbin/arp -a {$_SERVER['REMOTE_ADDR']}");
		preg_match('/..:..:..:..:..:../', $mac, $matches);
		$this->mac = strtolower(@$matches[0]);
	}

	function save_to_log() {
		$log = file_get_contents(self::LOG_FILE);
		if(strpos($log, $this->mac) === false){
			file_put_contents(self::LOG_FILE, $this->mac."\n", FILE_APPEND + LOCK_EX);			
		}
	}

	function add_to_rules() {
		exec("sudo iptables -t mangle -L ".self::CHAIN, $output);		
		$exist = false;
		foreach ($output as $o) {
			if(strpos(strtolower($o), $this->mac) !== false){
				$exist = true;
				break;
			}
		}
		if(!$exist)
			exec("sudo iptables -I ".self::CHAIN." 1 -t mangle -m mac --mac-source '{$this->mac}' -j RETURN");
	}

	function get() {
		return $this->mac;
	}
}

class Api {
	private $eth_mac;
	private $user;
	private $domain;
		
	function __construct() {
		$this->eth_mac = trim(file_get_contents('/sys/class/net/eth0/address'));
		if(!($this->domain = trim(file_get_contents('./domain')))){
			die('domain is empty');
		}			
		$this->user = new UserMac();
	}   
	
	function analytics() {
		$uri = http_build_query([
			'eth_mac' => $this->eth_mac
		]);
		return "https://".$this->domain."/websites/analytics?".$uri;
	}
	
	function redirect_url() {    
		$uri = http_build_query([
			't' => base64_encode(json_encode([
				'eth_mac' => $this->eth_mac,
				'user_mac' => $this->user->get(),
			]))
		]);		
		return "https://".$this->domain."/aps/index2/?$uri";
	}			
}

class Adapter {   
	function get($url){
		$options = array(
			'http' => array(
				'method'  => 'GET',
			),
		);
		$context  = stream_context_create($options);
		return @file_get_contents($url, false, $context);
	}

	function post($url, $data){
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query($data),
			),
		);
		$context  = stream_context_create($options);
		return @file_get_contents($url, false, $context);
	}  
}

class Host {
	private $hosts;

	private function get($index){
		return explode(" ", $this->hosts[$index])[1];       
	}

	function __construct() {
		$this->hosts = file_get_contents('/etc/dnsmasq.hosts');
		$this->hosts = explode("\n", $this->hosts);
	}   

	function is_local_domain() {
		return $_SERVER['SERVER_NAME'] == $this->get(0);
	}
}

$api = new Api();
$adapter = new Adapter();
$userMac = new UserMac();
$host = new Host();

if (!$userMac->get()){
	die("mac is missing"); 
}

if ($host->is_local_domain()) {
	$cookies = [];
	foreach ($_COOKIE as $k => $time) {
		if(preg_match('/web4you-mcc-analytics-([0-9]+)/i', $k, $m)){
			$cookies[$m[1]] = $time;
		}
	}

	if($cookies){
		echo $adapter->post($api->analytics(), [
			'user_mac'=>$userMac->get(),
			'json'=>json_encode($cookies)
			], false);		
	}
	
	if (!empty($_GET['t']) && base64_decode($_GET['t']) == $userMac->get()) {
		$userMac->save_to_log();
		$userMac->add_to_rules();
		exec("sudo rmtrack {$_SERVER['REMOTE_ADDR']}");
		sleep(3);
		header("location:".urldecode($_GET['r']));
	}
}else{
	header("location:".$api->redirect_url());
	exit;
}