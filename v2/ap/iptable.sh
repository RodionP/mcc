#!/bin/sh
AP_IP_NET='192.168.42'
AP_IP="$AP_IP_NET.1"
CHAIN='portal'

echo "internet interface: "
read inet
echo "ap interface: "
read iap

echo "#!/bin/sh
iptables -t mangle -F
iptables -F
iptables -t nat -F
iptables -t raw -F

iptables -t mangle -N $CHAIN
iptables -t mangle -A PREROUTING -i $iap -p tcp -m tcp --dport 80 -j $CHAIN
awk 'BEGIN {} { system(\"iptables -t mangle -A $CHAIN -m mac --mac-source \"\$1\" -j RETURN\"); }' /var/www/users
iptables -t mangle -A $CHAIN -p tcp --dst mcc-apps.com --dport 80 -j RETURN
iptables -t mangle -A $CHAIN -p tcp --dst dealerlogic.net --dport 80 -j RETURN
iptables -t mangle -A $CHAIN -j MARK --set-mark 99

iptables -t nat -A PREROUTING -i $iap -p tcp -m mark --mark 99 -m tcp --dport 80 -j DNAT --to-destination $AP_IP
iptables -t nat -A POSTROUTING -o $inet -j MASQUERADE

iptables -A FORWARD -i $inet -o $iap -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i $iap -o $inet -j ACCEPT

ifconfig $iap $AP_IP netmask 255.255.255.0
" > /etc/rc.local