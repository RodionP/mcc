apt-get -y install hostapd dnsmasq apache2 php5 libapache2-mod-php5 resolvconf conntrack ssmtp

AP_IP_NET='192.168.42'
AP_IP="$AP_IP_NET.1"
CHAIN='portal'

echo "internet interface: "
read inet
echo "ap interface: "
read iap

echo "captive portal domain name: "
read domain

echo "$AP_IP $domain
$AP_IP cp.web4you.ca" > /etc/dnsmasq.hosts

echo "no-hosts
server=8.8.8.8
addn-hosts=/etc/dnsmasq.hosts
dhcp-range=$AP_IP_NET.10,$AP_IP_NET.150,12h" > /etc/dnsmasq.conf 

sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/' /etc/sysctl.conf
sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"

echo "SSID: "
read ssid

echo "channel: "
read channel

sed -i 's%#DAEMON_CONF=""%DAEMON_CONF="/etc/hostapd/hostapd.conf"%' /etc/default/hostapd
echo "interface=$iap
driver=nl80211
ssid=$ssid
channel=$channel" > /etc/hostapd/hostapd.conf

ifconfig $iap $AP_IP

service hostapd start 
service dnsmasq start
update-rc.d hostapd enable

a2enmod rewrite
service apache2 restart

rm -f /var/www/index.php
rm -f /var/www/.htaccess

cp ./ap/index.php /var/www/
cp ./ap/.htaccess /var/www/

chown -R www-data:www-data /var/www
chown -R www-data:www-data /var/www/index.php
chown -R www-data:www-data /var/www/.htaccess

echo "
iptables -t mangle -F
iptables -F
iptables -t nat -F
iptables -t raw -F

iptables -t mangle -N $CHAIN
iptables -t mangle -A PREROUTING -i $iap -p tcp -m tcp --dport 80 -j $CHAIN
awk 'BEGIN { FS=\"\t\"; } { system(\"iptables -t mangle -A $CHAIN -m mac --mac-source \"\$4\" -j RETURN\"); }' /var/www/users
iptables -t mangle -A $CHAIN -p tcp --dst mcc-apps.com --dport 80 -j RETURN
iptables -t mangle -A $CHAIN -p tcp --dst barcode.tec-it.com --dport 80 -j RETURN
iptables -t mangle -A $CHAIN -j MARK --set-mark 99

iptables -t nat -A PREROUTING -i $iap -p tcp -m mark --mark 99 -m tcp --dport 80 -j DNAT --to-destination $AP_IP
iptables -t nat -A POSTROUTING -o $inet -j MASQUERADE

iptables -A FORWARD -i $inet -o $iap -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i $iap -o $inet -j ACCEPT

ifconfig $iap $AP_IP netmask 255.255.255.0
" > /etc/rc.local

echo "root=
mailhub=tra.tradeshowconnection.com:465
rewriteDomain=mcc-apps.com
hostname=mcc-apps.com
FromLineOverride=YES
AuthUser=wifi@mcc-apps.com
AuthPass=}JftD]]L9;Co
UseTLS=YES" > /etc/ssmtp/ssmtp.conf

echo "Add manualy:

/**********copy*******

auto lo
iface lo inet loopback

allow-hotplug $iap
iface $iap inet static
        address $AP_IP
        netmask 255.255.255.0

/**********copy*******

#nano /etc/network/interfaces"

cp rmtrack /usr/bin/

echo "add to sudo:

/*********copy************

www-data ALL=NOPASSWD: /usr/sbin/arp
www-data ALL=NOPASSWD: /sbin/iptables
www-data ALL=NOPASSWD: /usr/bin/rmtrack
www-data ALL=NOPASSWD: /usr/sbin/conntrac

"

echo "Change Apache config. Add Override All for /var/www"