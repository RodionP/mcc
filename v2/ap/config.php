<?php

require dirname(__FILE__)."/../adapter.php";
require dirname(__FILE__)."/../hostpad.php";

class Dns {
 	const FILE = '/etc/dnsmasq.hosts'; 
 	private $changed;
 	private $dnsmasq;
 	
	function __construct() {
		$this->dnsmasq = [];
		if(file_exists(self::FILE)){
			$items = file_get_contents(self::FILE);
			$items = explode("\n", trim($items));
			foreach ($items as $i) {
				list($ip, $d) = explode(" ", $i);
				$this->dnsmasq[] = [$ip, $d];
			}
		}
	}   

	function set($config) {
		$this->changed = 0;
		if(preg_match('/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/', $config->domain) 
			&& $this->dnsmasq[0][1] != $config->domain){
				
			$this->dnsmasq[0][1] = $config->domain;
			$this->changed++;
		}
	}
	
	function save() {	
		if($this->changed > 0){
			$c = [];
			foreach ($this->dnsmasq as $i) {
				$c[] = "{$i[0]} {$i[1]}";
			}
			file_put_contents(self::FILE, join("\n", $c));			
		}
		return $this->changed > 0;
	}
	
	function restart() {
		system('/usr/sbin/service dnsmasq stop');
		sleep(3);
		system('/usr/sbin/service dnsmasq start');
		sleep(3);
	}
	
	function get_domain(){
		return $this->dnsmasq[0][1];
	}
}

class Iptables {   	
	private $macs = [];
	
	function __construct() {
		$this->reload();
	}   
	
	function reload() {
		$this->macs = [];
		$iptables = shell_exec("/sbin/iptables -t mangle -L -nvx");
		if(preg_match_all('/MAC ([0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2})/ism', 
			$iptables, $matches)){
			$this->macs = $matches[1];
		}		
	}      
		
	function add($mac) {	
		$mac = strtoupper($mac);
		if(!in_array($mac, $this->macs)){
			shell_exec("/sbin/iptables -t mangle -I portal 1 -m mac --mac-source \"$mac\" -j RETURN");
			array_unshift($this->macs, $mac);
			$this->macs = array_values($this->macs);
			return true;
		}	   	
		return false;
	}
	
	function delete($mac) {	
		$mac = strtoupper($mac);
		$index = array_search($mac, $this->macs);
		if($index !== false){
			$index++;
			shell_exec("/sbin/iptables -t mangle -D portal $index");
			$this->macs = array_values(array_diff($this->macs, [$mac]));
			return true;
		}		
		return false;
	}
	
	function macs(){
		return $this->macs;
	}
	
	private function validate($macs, $in=true){
		$macs = array_map('strtoupper', $macs);
	   $valid = true;
	   foreach ($macs as $m) {
	   	if($in && in_array($m, $this->macs) || 
	   		!$in && !in_array($m, $this->macs)){
				$valid = false;
				break;
	   	}
	   }
		return $valid;		
	}
	function validate_delete($deleted) {		
		return $this->validate($deleted, true);
	}		
	
	function validate_add($added) {
		return $this->validate($added, false);
	}			
}

$aps = new ApsAdapter();
$hostpad = new Hostpad();
$dns = new Dns();

if(!($config = json_decode($aps->get())))
	return;
echo "config loaded: ".print_r($config, true)."\n";
	
$hostpad->set($config);
$dns->set($config);
	
if($hostpad->save()){	
	echo "restarting hostpad...\n";
	$hostpad->restart();
	$info = shell_exec("/sbin/iw {$hostpad->get_interface()} info");
	echo $info;
	$aps->post($info);		
	echo "ap channel set to: {$hostpad->get_channel()}\n";
	echo "ap ssid set to: {$hostpad->get_ssid()}\n";
}else{
	echo "ap is up to date\n";
}

if($dns->save()){
	echo "restarting dnsmasq...\n";
	$dns->restart();
	$info = shell_exec("/usr/bin/host {$dns->get_domain()}");
	echo $info;
	$aps->post($info);		
	echo "portal domain is set to: {$dns->get_domain()}\n";
}else{
	echo "portal domain is up to date\n";
}

$sync = new SyncAdapter();
if($data = $sync->get()){
	if($data = json_decode($data)){
		echo "sync macs...\n";		
		print_r($data);
		
		$log_file = '/var/www/users';
		file_put_contents('timestamp', $data->timestamp);
		$users = file_exists($log_file) ? array_filter(array_unique(explode("\n", file_get_contents($log_file)))) : [];
		$users = array_filter($users, function($m){
			return preg_match('/^[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}$/i', $m, $matches);
		});
		
		foreach ($data->macs as $m => $deleted) {
			if($deleted){				
				$users = array_diff($users, [$m]);
			}else{
				if(!in_array($m, $users))
					$users[] = $m;
			}        
		}
		file_put_contents($log_file, join("\n", $users));
		
		$iptables = new Iptables();
		$added = [];
		$deleted = [];
		foreach ($data->macs as $m => $is_deleted) {
			if($is_deleted){
				if($iptables->delete($m)){
					$deleted[] = $m;
					echo "deleted: $m\n";					
				}
			}else{
				if($iptables->add($m)){
					$added[] = $m;
					echo "added: $m\n";					
				}
			}
		}
		$iptables->reload();
		$json = [];
		if($iptables->validate_delete($deleted)){
			$json['deleted'] = $deleted;
		}
		if($iptables->validate_add($added)){
			$json['added'] = $added;
		}
		echo "propagate sync...\n";
		print_r($json);
		$sync->post(json_encode($json));
	}
}